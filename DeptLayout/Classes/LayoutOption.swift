import UIKit

public enum LayoutOption {
    case alpha(CGFloat)
    case attributedPlaceholder(NSAttributedString)
    case backgroundImage(UIImage)
    case backgroundColor(UIColor)
    case borderColor(CGColor)
    case borderWidth(CGFloat)
    case attributedTitle(NSAttributedString)
    case title(String)
    indirect case controlState(UIControlState, LayoutOption)
    case clip(Bool)
    case color(UIColor)
    case contentMode(UIViewContentMode)
    case corner(CGFloat)
    case font(UIFont)
    case headIndent(CGFloat)
    case height(CGFloat)
    case hidden(Bool)
    case image(UIImage?)
    case kern(CGFloat)
    case lineBreakMode(NSLineBreakMode)
    case lineHeight(CGFloat)
    case numberOfLines(Int)
    case preferredWidth(CGFloat)
    case tabs([NSTextTab])
    case textAlignment(NSTextAlignment)
    case tintColor(UIColor)
    case width(CGFloat)
    
    
    public func toAttributedName() -> [AttributedStringKey: Any] {
        switch self {
        case let .color(color):
            return [AttributedStringKey.foregroundColor : color]
        case let .font(font):
            return [AttributedStringKey.font : font]
        case let .kern(kern):
            return [AttributedStringKey.kern : kern]
        default:
            break
        }
        return [AttributedStringKey: Any]()
    }
}

