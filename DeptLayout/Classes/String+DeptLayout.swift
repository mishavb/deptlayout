//
//  String+Alliantie.swift
//  DeAlliantie Housing App
//
//  Created by Misha van Broekhoven on 19/10/16.
//  Copyright © 2016 Infinum. All rights reserved.
//

import UIKit

public class InlineTextAttachment: NSTextAttachment {
    var fontDescender: CGFloat = 0
    
    override public func attachmentBounds(
        for textContainer: NSTextContainer?,
        proposedLineFragment lineFrag: CGRect,
        glyphPosition position: CGPoint,
        characterIndex charIndex: Int) -> CGRect {
        var superRect = super.attachmentBounds(
            for: textContainer,
            proposedLineFragment: lineFrag,
            glyphPosition: position,
            characterIndex: charIndex)
        superRect.origin.y = fontDescender
        return superRect
    }
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        #if swift(>=4)
            return String(self[start ..< end])
        #else
            return String(self[Range(start ..< end)])
        #endif
    }
    
    subscript (r: ClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        #if swift(>=4)
            return String(self[start ..< end])
        #else
            return String(self[Range(start ..< end)])
        #endif
    }
}

extension String {
    
    public var attributed: NSMutableAttributedString {
        return NSMutableAttributedString(string:self)
    }
    
    public func attributed(_ layout: Layout, _ newLine: Bool = false) -> NSMutableAttributedString {
        let attributed = NSMutableAttributedString(string:newLine == true ? "\(self)\n" : self)
        attributed.apply(layout)
        return attributed
    }
    
    public static func attributed(_ layout: Layout, _ newLine: Bool = false) -> NSMutableAttributedString {
        return "".attributed(layout, newLine)
    }
    
    
    public static func space(height: CGFloat, _ newLine: Bool = true) -> NSMutableAttributedString {
        let textAttachment = NSTextAttachment()
        textAttachment.image = UIImage(color: UIColor.clear)
        textAttachment.bounds = CGRect(x: 0, y: 0, width: 1, height: height)
        if let image = NSAttributedString(attachment: textAttachment).mutableCopy() as? NSMutableAttributedString {
            if newLine {
                image.append(NSAttributedString(string: "\n"))
            }
            return image
        }
        return NSMutableAttributedString()
    }
    
    public static func hairline(_ color: UIColor = .lightGray, _ width: CGFloat) -> NSMutableAttributedString {
        let textAttachment = NSTextAttachment()
        textAttachment.image = UIImage.init(color: color)
        textAttachment.bounds = CGRect(x: 0, y: 0, width: width, height: 0.75)
        if let image = NSAttributedString(attachment: textAttachment).mutableCopy() as? NSMutableAttributedString {
            return image
        }
        return NSMutableAttributedString()
    }
    
    public static func image(_ image: UIImage,
                             _ layout: Layout,
                             width: CGFloat = 0,
                             height: CGFloat = 0) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let attr = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(attributedString: attr)
        string.apply(layout)
        return string
        
    }
    
    public static func image(_ image: UIImage,
                             _ layout: Layout, width: CGFloat = 0, height: CGFloat = 0, y: CGFloat = 0) -> NSMutableAttributedString {
        let attachment = InlineTextAttachment()
        attachment.fontDescender = y
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let attr = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(attributedString: attr)
        string.apply(layout)
        return string
        
    }
    
    
    public static func combine(_ attributes: [NSMutableAttributedString]) -> NSMutableAttributedString {
        return attributes.reduce("".attributed, { (combined, attributed) -> NSMutableAttributedString in
            combined.append(attributed)
            return combined
        })
    }
    
    public func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat(Float.infinity))
        let boundingBox = (self as NSString).boundingRect(
            with: constraintRect,
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [AttributedStringKey.font: font],
            context: nil)
        return boundingBox.height
    }
    
}
