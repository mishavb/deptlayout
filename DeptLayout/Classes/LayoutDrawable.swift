import UIKit

//MARK: - LayoutDrawable
public protocol LayoutDrawable {
    
    func apply(_ layout: Layout)
    
}

//MARK: - LayoutView
open class LayoutView : UIView, LayoutDrawable {
    
    open func apply(_ layout: Layout) {
        for option in layout.options {
            switch option {
            case let .alpha(alpha):
                self.alpha = alpha
            case let .backgroundColor(backgroundColor):
                self.backgroundColor = backgroundColor
            case let .borderColor(color):
                layer.borderColor = color
            case let .borderWidth(width):
                layer.borderWidth = width
            case let .hidden(isHidden):
                self.isHidden = isHidden
            case let .tintColor(tintColor):
                self.tintColor = tintColor
            case let .height(height):
                frame.size.height = height
                constraintTo(metric: .heightEqual(height))
            case let .corner(radius):
                self.layer.cornerRadius = radius
            case let .width(width):
                frame.size.width = width
                constraintTo(metric: .widthEqual(width))
            default:
                break
            }
        }
    }
    
}

//MARK: - UITextField
extension UITextField : LayoutDrawable {
    
    public func apply(_ layout: Layout) {
        for option in layout.options {
            switch option {
            case let .alpha(alpha):
                self.alpha = alpha
            case let .borderColor(color):
                layer.borderColor = color
            case let .borderWidth(width):
                layer.borderWidth = width
            case let .corner(radius):
                self.layer.cornerRadius = radius
            case let .textAlignment(alignment):
                self.textAlignment = alignment
            case let .backgroundColor(backgroundColor):
                self.backgroundColor = backgroundColor
            case let .font(font):
                self.font = font
            case let .hidden(isHidden):
                self.isHidden = isHidden
            case let .tintColor(tintColor):
                self.tintColor = tintColor
            case let .height(height):
                constraintTo(metric: .heightEqual(height))
            case let .width(width):
                constraintTo(metric: .widthEqual(width))
            default:
                break
            }
        }
    }
    
}

//MARK: - UILabel
extension UILabel : LayoutDrawable {
    
    public convenience init(frame: CGRect, style : Layout) {
        self.init(frame: frame)
        self.apply(style)
    }
    
    public func apply(_ layout: Layout) {
        for option in layout.options {
            switch option {
            case let .color(color):
                textColor = color
            case let .alpha(alpha):
                self.alpha = alpha
            case let .borderColor(color):
                layer.borderColor = color
            case let .borderWidth(width):
                layer.borderWidth = width
            case let .clip(toBounds):
                self.clipsToBounds = toBounds
            case let .corner(radius):
                self.layer.cornerRadius = radius
            case let .textAlignment(alignment):
                self.textAlignment = alignment
            case let .backgroundColor(backgroundColor):
                self.backgroundColor = backgroundColor
            case let .font(font):
                self.font = font
            case let .hidden(isHidden):
                self.isHidden = isHidden
            case let .lineBreakMode(lineBreakMode):
                self.lineBreakMode = lineBreakMode
            case let .numberOfLines(numberOfLines):
                self.numberOfLines = numberOfLines
            case let .tintColor(tintColor):
                self.tintColor = tintColor
            case let .height(height):
                constraintTo(metric: .heightEqual(height))
            case let .width(width):
                constraintTo(metric: .widthEqual(width))
            case let .preferredWidth(width):
                self.preferredMaxLayoutWidth = width
            default:
                break
            }
        }
    }
    
}

//MARK: - UIButton
extension UIButton: LayoutDrawable {
    
    fileprivate func apply(controlstate: UIControlState, option: LayoutOption) {
        switch option {
        case let .color(color):
            setTitleColor(color, for: controlstate)
        case let .image(image):
            setImage(image, for: controlstate)
        case let .backgroundImage(image):
            setBackgroundImage(image, for: controlstate)
        case let .attributedTitle(title):
            setAttributedTitle(title, for: controlstate)
        case let .title(title):
            setTitle(title, for: controlstate)
        default:
            break
        }
    }
    
    fileprivate func applyColor(option: LayoutOption) {
        switch option {
        case let .backgroundColor(backgroundColor):
            self.backgroundColor = backgroundColor
        case let .borderColor(color):
            layer.borderColor = color
        case let .tintColor(tintColor):
            self.tintColor = tintColor
        default:
            break
        }
    }
    
    fileprivate func applyText(option: LayoutOption) {
        switch option {
        case let .numberOfLines(lines):
            titleLabel?.numberOfLines = lines
        case let .lineBreakMode(mode):
            titleLabel?.lineBreakMode = mode
        case let .textAlignment(alignment):
            titleLabel?.textAlignment = alignment
        default:
            break
        }
    }
    
    fileprivate func applyConstraints(option: LayoutOption) {
        switch option {
        case let .height(height):
            constraintTo(metric: .heightEqual(height))
        case let .width(width):
            constraintTo(metric: .widthEqual(width))
        default:
            break
        }
    }
    
    fileprivate func applyButton(option: LayoutOption) {
        let states: [UIControlState] = [.normal, .highlighted, .selected ]
        switch option {
        case let .color(color):
            for state in states {
                setTitleColor(color, for: state)
            }
        case let .title(title):
            for state in states {
                setTitle(title, for: state)
            }
        case let .attributedTitle(title):
            for state in states {
                setAttributedTitle(title, for: state)
            }
        case let .backgroundImage(image):
            for state in states {
                setBackgroundImage(image, for: state)
            }
        case let .image(image):
            for state in states {
                setImage(image, for: state)
            }
        default:
            break
        }
    }
    
    public func apply(_ layout: Layout) {
        for option in layout.options {
            switch option {
            case let .controlState(state, option):
                apply(controlstate : state, option: option)
            case let .alpha(alpha):
                self.alpha = alpha
            case let .borderWidth(width):
                layer.borderWidth = width
            case let .clip(toBounds):
                self.clipsToBounds = toBounds
            case let .corner(radius):
                self.layer.cornerRadius = radius
                self.clipsToBounds = true
            case let .font(font):
                titleLabel?.font = font
            case let .hidden(isHidden):
                self.isHidden = isHidden
            case let .contentMode(contentMode):
                self.contentMode = contentMode
            default:
                applyButton(option: option)
                applyColor(option: option)
                applyText(option: option)
                applyConstraints(option: option)
                break
            }
        }
    }
    
}

//MARK: - UIImageView
extension UIImageView: LayoutDrawable {
    
    public func apply(_ layout: Layout) {
        for option in layout.options {
            switch option {
            case let .alpha(alpha):
                self.alpha = alpha
            case let .backgroundColor(backgroundColor):
                self.backgroundColor = backgroundColor
            case let .borderColor(color):
                layer.borderColor = color
            case let .borderWidth(width):
                layer.borderWidth = width
            case let .corner(radius):
                self.layer.cornerRadius = radius
            case let .hidden(isHidden):
                self.isHidden = isHidden
            case let .image(image):
                self.image = image
            case let .contentMode(contentMode):
                self.contentMode = contentMode
            case let .tintColor(tintColor):
                self.tintColor = tintColor
            case let .height(height):
                constraintTo(metric: .heightEqual(height))
            case let .width(width):
                constraintTo(metric: .widthEqual(width))
            default:
                break
            }
        }
    }
    
}

//MARK: - NSMutableAttributedString
extension NSMutableAttributedString : LayoutDrawable {
    
    public convenience init(string: String, layout : Layout) {
        self.init(string: string, attributes:layout.attributes())
    }
    
    public func apply(_ layout: Layout) {
        let range = NSRange(location: 0, length: string.count)
        addAttributes(layout.attributes(), range: range)
    }
    
    public func apply(_ layout: Layout, range : NSRange) {
        addAttributes(layout.attributes(), range: range)
    }
    
}
