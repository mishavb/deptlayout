import UIKit

#if swift(>=4.2)
public typealias UIViewContentMode = UIView.ContentMode
public typealias UIControlState = UIControl.State
public typealias AttributedStringKey = NSAttributedString.Key
public typealias LayoutRelation = NSLayoutConstraint.Relation
public typealias LayoutAttribute = NSLayoutConstraint.Attribute
public typealias UITableViewRowAnimation = UITableView.RowAnimation
public typealias UIViewAnimationOptions = UIView.AnimationOptions
public let KeyboardWillShowNotification = UIResponder.keyboardWillShowNotification
public let KeyboardWillHideNotification = UIResponder.keyboardWillHideNotification
public let KeyboardFrameBeginUserInfoKey = UIResponder.keyboardFrameBeginUserInfoKey
@available(iOS 11.0, *)
public typealias UIScrollViewContentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior
#else
public typealias AttributedStringKey = NSAttributedStringKey
public typealias LayoutRelation = NSLayoutRelation
public typealias LayoutAttribute = NSLayoutAttribute
public let KeyboardWillShowNotification = NSNotification.Name.UIKeyboardWillShow
public let KeyboardWillHideNotification = NSNotification.Name.UIKeyboardWillHide
public let KeyboardFrameBeginUserInfoKey = UIKeyboardFrameBeginUserInfoKey
#endif

public protocol LayoutInterface {
    var options : [LayoutOption] { get set }
    
    init(_ options : [LayoutOption])
    
    func add(_ option : LayoutOption) -> Self
    func add(_ options : [LayoutOption]) -> Self
    
    func attributes() -> [AttributedStringKey : Any]
}

public class Layout : LayoutInterface {
    public var options : [LayoutOption]
    
    fileprivate var attr = [AttributedStringKey : Any]()
    fileprivate let paragraph = NSMutableParagraphStyle()
    
    public required init(_ options : [LayoutOption]) {
        self.options = options
    }
    
    public func add(_ options : [LayoutOption]) -> Self {
        options.forEach({ _ = add($0) })
        return self
    }
    
    public func add(_ option : LayoutOption) -> Self {
        self.options.append(option)
        switch option {
        case .height(_), .width(_):
            break
        case let .lineBreakMode(lineBreakMode):
            paragraph.lineBreakMode = lineBreakMode
            addAttribute(option)
        case let .tabs(tabs):
            paragraph.tabStops = tabs
        case let .lineHeight(lineHeight):
            paragraph.lineHeightMultiple = lineHeight
        case let .textAlignment(alignment):
            paragraph.alignment = alignment
        case let .headIndent(headIndent):
            paragraph.headIndent = headIndent
        default:
            addAttribute(option)
        }
        return self
    }
    
    private func addAttribute(_ option : LayoutOption) {
        let attribute = option.toAttributedName()
        if attribute.count == 0 {
            return
        }
        self.attr.removeValue(forKey: attribute.keys.first!)
        self.attr[attribute.keys.first!] = attribute.values.first!
    }
    
    //NOTE: NSAttributedString Attributes
    public func attributes() -> [AttributedStringKey: Any] {

        for option in self.options {
            _ = add(option)
        }
        attr[AttributedStringKey.paragraphStyle] = paragraph
        return attr
    }
}



