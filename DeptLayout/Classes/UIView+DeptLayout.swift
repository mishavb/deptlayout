import UIKit

public enum ConstraintEdge {
    case left(CGFloat, LayoutRelation)
    case right(CGFloat, LayoutRelation)
    case bottom(CGFloat, LayoutRelation)
    case top(CGFloat, LayoutRelation)
    
    public func constaintFor(item: UIView,
                             toItem: UIView?,
                             attribute: LayoutAttribute,
                             relation: LayoutRelation,
                             constant: CGFloat) {
        let toAttribute: LayoutAttribute = toItem == nil ? .notAnAttribute : attribute
        let constraint = NSLayoutConstraint(item: item,
                                            attribute: attribute,
                                            relatedBy: relation,
                                            toItem: toItem,
                                            attribute: toAttribute,
                                            multiplier: 1.0,
                                            constant: constant)
        
        if toItem == nil {
            item.addConstraint(constraint)
        } else {
            toItem?.addConstraint(constraint)
        }
    }
    
    public func constraint(view: UIView, superview: UIView?) {
        view.translatesAutoresizingMaskIntoConstraints = false
        switch self {
        case let .top(constant, relation):
            constaintFor(item: view, toItem: superview, attribute: .top, relation: relation, constant: constant)
        case let .bottom(constant, relation):
            constaintFor(item: view, toItem: superview, attribute: .bottom, relation: relation, constant: constant)
        case let .left(constant, relation):
            constaintFor(item: view, toItem: superview, attribute: .left, relation: relation, constant: constant)
        case let .right(constant, relation):
            constaintFor(item: view, toItem: superview, attribute: .right, relation: relation, constant: constant)
        }
    }
}

public enum ConstraintMetric {
    case widthEqual(CGFloat)
    case heightEqual(CGFloat)
    
    public func constraintMetric(_ item: UIView,
                                 _ attribute: LayoutAttribute,
                                 _ relation: LayoutRelation,
                                 _ constant: CGFloat) {
        let constraint = NSLayoutConstraint(item: item,
                                            attribute: attribute,
                                            relatedBy: relation,
                                            toItem: nil,
                                            attribute: .notAnAttribute,
                                            multiplier: 1.0,
                                            constant: constant)
        item.addConstraint(constraint)
    }
    
    public func constraint(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        switch self {
        case let .widthEqual(width):
            constraintMetric(view, .width, .equal, width)
        case let .heightEqual(height):
            constraintMetric(view, .height, .equal, height)
        }
    }
}

extension UIView {
    
    typealias Constraint = NSLayoutConstraint
    
    public func constraintToEdges() {
        guard let superview = self.superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        let views = ["view": self]
        let vertical = Constraint.constraints(
            withVisualFormat: "V:|[view]|",
            options: .alignAllLastBaseline,
            metrics: nil,
            views: views)
        let horizontal = Constraint.constraints(
            withVisualFormat: "H:|[view]|",
            options: .alignAllLastBaseline,
            metrics: nil,
            views: views)
        superview.addConstraints(vertical)
        superview.addConstraints(horizontal)
    }
    
    public func constaintTo(superview: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        superview.addSubview(self)
        constraintToEdges()
    }
    
    public func constraintTo(superview: UIView? = nil, edges: [ConstraintEdge]) {
        for edge in edges {
            constraintTo(superview: superview, edge: edge)
        }
    }
    
    public func constraintTo(superview: UIView? = nil, edge: ConstraintEdge) {
        edge.constraint(view: self, superview: superview ?? self.superview)
    }
    
    public func constraintTo(metrics: [ConstraintMetric]) {
        for metric in metrics {
            metric.constraint(view: self)
        }
    }
    
    public func constraintTo(metric: ConstraintMetric) {
        metric.constraint(view: self)
    }
    
}
