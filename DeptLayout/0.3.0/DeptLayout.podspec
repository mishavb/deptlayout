#
# Be sure to run `pod lib lint DeptLayout.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DeptLayout'
  s.version          = '0.3.0'
  s.summary          = 'Define easy styles that can be applied to buttons, views, labels and attributed strings.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Define easy styles that can be applied to buttons, views, labels and attributed strings.
By creating Layout objects which work like css styles.
                       DESC

  s.homepage         = 'https://bitbucket.org/mishavb/deptlayout'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Misha van Broekhoven' => 'misha@tamtam.nl' }
  s.source           = { :git => 'https://bitbucket.org/mishavb/deptlayout.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'DeptLayout/Classes/**/*'
  
  # s.resource_bundles = {
  #   'DeptLayout' => ['DeptLayout/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
