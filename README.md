# DeptLayout

[![CI Status](http://img.shields.io/travis/Misha van Broekhoven/DeptLayout.svg?style=flat)](https://travis-ci.org/Misha van Broekhoven/DeptLayout)
[![Version](https://img.shields.io/cocoapods/v/DeptLayout.svg?style=flat)](http://cocoapods.org/pods/DeptLayout)
[![License](https://img.shields.io/cocoapods/l/DeptLayout.svg?style=flat)](http://cocoapods.org/pods/DeptLayout)
[![Platform](https://img.shields.io/cocoapods/p/DeptLayout.svg?style=flat)](http://cocoapods.org/pods/DeptLayout)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DeptLayout is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DeptLayout"
```

## Author

Misha van Broekhoven, misha@tamtam.nl

## License

DeptLayout is available under the MIT license. See the LICENSE file for more info.
